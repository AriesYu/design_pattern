package design_pattern.proxy;

/**
 * @author Aries.Yu
 */
public interface GiveGift {
    public abstract void giveFlowers();
    public abstract void giveDrinks();
    public abstract void giveDolls();
}
