package design_pattern.proxy;

/**
 * @author Aries.Yu
 */
public class RealPursuit implements GiveGift {

    GirlToGiveGifts girl;

    public RealPursuit(GirlToGiveGifts girl) {
        this.girl = girl;
    }

    @Override
    public void giveFlowers() {
        System.out.println("give "+girl.getName()+" flowers" );
    }

    @Override
    public void giveDrinks() {
        System.out.println("give "+girl.getName()+" drinks" );
    }

    @Override
    public void giveDolls() {
        System.out.println("give "+girl.getName()+" dolls" );
    }
}
