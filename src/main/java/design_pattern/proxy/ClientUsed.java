package design_pattern.proxy;

/**
 * @author Aries.Yu
 */
public class ClientUsed {
    public static void main(String[] args) {

        GirlToGiveGifts girl = new GirlToGiveGifts("Sandy");
        Proxy proxy = new Proxy(girl);
        proxy.giveDolls();
        proxy.giveDrinks();
        proxy.giveFlowers();
    }
}
