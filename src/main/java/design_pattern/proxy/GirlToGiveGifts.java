package design_pattern.proxy;

/**
 * @author Aries.Yu
 */
public class GirlToGiveGifts {

    private String name;

    public GirlToGiveGifts(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
