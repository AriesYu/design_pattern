package design_pattern.proxy;

/**
 * @author Aries.Yu
 */
public class Proxy implements GiveGift {


    RealPursuit realPursuit;

    public Proxy(GirlToGiveGifts girl) {
        realPursuit = new RealPursuit(girl);

    }

    @Override
    public void giveFlowers() {
        realPursuit.giveFlowers();
    }

    @Override
    public void giveDrinks() {
        realPursuit.giveDrinks();
    }

    @Override
    public void giveDolls() {
        realPursuit.giveDolls();
    }
}
