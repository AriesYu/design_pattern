package design_pattern.factory_method;

/**
 * @author Aries.Yu
 */
public class ClinetOfFactoryMethod {
    public static void main(String[] args) {
        OperationGenerator addFactory = new AddFactory();
        OperationExecutor addOperation = addFactory.generateOperation();
        System.out.println(addOperation.getResult(3, 4));

        OperationGenerator subFacotry = new SubFactory();
        OperationExecutor subOperation = subFacotry.generateOperation();
        System.out.println(subOperation.getResult(5, 4));

    }
}
