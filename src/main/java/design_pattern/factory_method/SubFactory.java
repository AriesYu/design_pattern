package design_pattern.factory_method;

/**
 * @author Aries.Yu
 */
public class SubFactory implements  OperationGenerator{

    @Override
    public OperationExecutor generateOperation() {
        return new SubOperation();
    }
}
