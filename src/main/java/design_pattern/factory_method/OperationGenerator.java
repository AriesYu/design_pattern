package design_pattern.factory_method;

/**
 * @author Aries.Yu
 */
public interface OperationGenerator {
    public OperationExecutor generateOperation();
}
