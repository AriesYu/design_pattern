package design_pattern.factory_method;

/**
 * @author Aries.Yu
 */
public class AddFactory implements  OperationGenerator{
    @Override
    public OperationExecutor generateOperation() {
        return new AddOperation();
    }
}
