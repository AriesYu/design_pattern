package design_pattern.factory_method;

/**
 * @author Aries.Yu
 */
public class SubOperation implements OperationExecutor{
    @Override
    public long getResult(long numberA, long numberB) {
        return numberA - numberB;
    }
}
