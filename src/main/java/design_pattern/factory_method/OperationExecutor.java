package design_pattern.factory_method;

/**
 * @author Aries.Yu
 */
public interface OperationExecutor {
    public long getResult(long numberA,long numberB);
}
